<?php
namespace Space10\EventManager;

use Zend\EventManager\EventInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\Mvc\MvcEvent;

/**
 * Class Module
 * @package Space10\EventManager
 */
class Module implements BootstrapListenerInterface
{

    /**
     * Listen to the bootstrap event
     *
     * @param EventInterface $e
     * @return array
     */
    public function onBootstrap(EventInterface $e)
    {
        if (!$e instanceof MvcEvent) {
            return;
        }

        $eventManager = $e->getApplication()->getEventManager();
        $sm = $e->getApplication()->getServiceManager();
        $config = $sm->get('config');

        if (array_key_exists('event_manager', $config) && is_array($config['event_manager']) && array_key_exists('listener', $config['event_manager']) && is_array($config['event_manager']['listener']))
        {
            $listeners = $config['event_manager']['listener'];
            foreach ($listeners as $curListener)
            {
                $listener = $sm->get($curListener);
                $eventManager->attach($listener);
            }
        }
    }
}